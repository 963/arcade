#include "setup.h"
#include "lists.h"
#include "block_blast.h"
#include "hedgehog.h"
// #include "ricochet.h"
#include "snake.h"
#include "space_trip.h"
#include <cassert>



Setup::Setup(Screen &screen_in, Scores &scores_in)
: screen{screen_in}, scores{scores_in} { }



int Setup::play(size_t game_index, size_t difficulty_index) const {
	assert(difficulty_index < DIFFICULTY_LIST.size());

	Game *game = nullptr;

	switch(game_index) {
		case 0:
			game = new Block_blast(screen);
			break;
		case 1:
			game = new Hedgehog(screen);
			break;
//		case 2:
//			game = new Ricochet(screen);
//			break;
//		case 3:
//			game = new Snake(screen);
//			break;
//		case 4:
//			game = new Space_trip(screen);
//			break;
		case 2:
			game = new Snake(screen);
			break;
		case 3:
			game = new Space_trip(screen);
			break;
		default:
			assert(false);
	}

	int new_score = game->play(difficulty_index);
	scores.push(game_index, difficulty_index, new_score);
	scores.save();

	delete game;
	return new_score;
}
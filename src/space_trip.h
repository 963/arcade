#ifndef SPACE_TRIP_H
#define SPACE_TRIP_H

#include "speed_game.h"
#include "point.h"
#include "randomizer.h"
#include <string>
#include <vector>
#include <queue>



class Space_trip : public Speed_game {
public:

	// M: this
	// E: Creates a space_trip attached to given screen and keyboard
	Space_trip(Screen &screen_in);

	// M: this
	// E: Destroys this
	virtual ~Space_trip();

private:

	// Types

	struct Bullet {
		Point<size_t> pos;
		bool boom;

		Bullet();
		Bullet(size_t row_in, size_t col_in, bool boom_in);
	};

	enum class Direction {UP, DOWN};

	// Variables

	static const std::string MODULE_FILE;
	static const char DELIM;
	static const char PLAYER;
	static const char WEAPON;
	static const char BOOM;
	static const size_t BAR_ROWS;
	static const size_t CHARGE_TIME;
	static const size_t LASER_COST;
	static const size_t MOD_DISTANCE;
	static const size_t CURVE_START;
	static const size_t CURVE;

	std::vector<std::vector<std::string>> modules;
	std::vector<std::string> station;
	std::vector<Bullet> bullets;
	Randomizer ran;
	size_t time, power, player_pos, mods_placed, want_bullets;
	std::queue<Direction> want_move;
	bool want_laser;

	// Functions

	// M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() override;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) override;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) override;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() override;

	// R: File MODULE_FILE exists and is correctly formatted
	// M: this->modules
	// E: Loads this->modules from MODULE_FILE
	void load_modules();

	// R: modules are loaded
	// E: Checks that all modules are rectangular, returns true iff all good
	bool check_modules() const;

	// R: modules are loaded
	// M: this->station
	// E: Moves the station left, adding a new module if necessary
	void move_station();

	// M: this-bullets
	// E: Removes all boomed bullets
	void boom_bullets();

	// R: power > 0
	// M: this->station, this->power, this->bullets
	// E: Adds a bullet to bullets, consumes power
	void fire_bullet();

	// M: this->station, this->bullets
	// E: Checks for collisions between bullets and station, booms collided
	//    bullets
	void collide_bullets();

	// M: this->bullets
	// E: Moves all non-boomed bullets one space to the right, removes bullets
	//    that go past the edge of the screen
	void move_bullets();

	// R: Station is drawn on screen
	// M: screen
	// E: Draws bullets on screen
	void draw_bullets();

	// R: station is drawn on screen, power >= LASER_COST
	// M: this->station, this->power, screen
	// E: Fires a laser, erasing a row of the station, drawing a cool line on
	//    screen, and consuming power
	void fire_laser();

	// R: station is drawn on screen
	// M: this->station, this->power, screen
	// E: Moves the player want_move rows down, checks for player collisions
	//    along the way and resolves it if necessary, draws player, returns true
	//    iff player survived
	bool move_player();

	// R: Station is drawn on screen
	// M: this->station, this->power, screen
	// E: Checks for collision between player and station, resolves it if
	//    necessary, return true iff player survived
	bool collide_player();

	// M: screen
	// E: Updates the info bar at the top
	void update_bar();

};

#endif
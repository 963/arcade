#ifndef SPEED_GAME_H
#define SPEED_GAME_H

#include "game.h"
#include <chrono>



class Speed_game : public Game {
public:

	// M: this
	// E: Creates a speed game attached to given screen and keyboard
	Speed_game(Screen &screen_in);

	// R: difficulty is valid
	// M: this, screen, keyboard
	// E: Plays the game at specified difficulty, returns the score
	virtual int play(size_t difficulty) override;

	// M: this
	// E: Destroys this
	virtual ~Speed_game();

protected:

	// Types

	using Interval = std::chrono::duration<double,std::milli>;

	// Variables

	int score, multiplier;

private:

	// M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() = 0;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) = 0;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) = 0;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() = 0;

	Speed_game() = delete;
};

#endif
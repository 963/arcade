#ifndef QUIT_H
#define QUIT_H


// Usage:
//     try {
//         ...
//         quit();
//         ...
//     } catch (Quit q) {
//         stop_program();
//     } catch (...) {
//         oops_thats_an_error();
//     }





#include <exception>



class Quit : public std::exception {};

void quit();

#endif
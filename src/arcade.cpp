#include "quit.h"
#include "screen.h"
#include "score.h"
#include "menu.h"
#include "setup.h"
#include <algorithm>
#include <iostream>
#include <exception>

using namespace std;





class Argument : public exception {};



int main(int argc, char **argv) {
	try {
		if (argc != 1 && argc != 3) {
			throw Argument{};
		}

		size_t difficulty_index = 0;
		size_t game_index = 0;

		if (argc == 3) {
			auto it_g = find(GAME_LIST.begin(), GAME_LIST.end(), argv[1]);
			if (it_g == GAME_LIST.end()) {
				throw Argument{};
			}
			game_index = it_g - GAME_LIST.begin();
			auto it_d = find(DIFFICULTY_LIST.begin(), DIFFICULTY_LIST.end(),
							argv[2]);
			if (it_d == DIFFICULTY_LIST.end()) {
				throw Argument{};
			}
			difficulty_index = it_d - DIFFICULTY_LIST.begin();
		}

		Screen screen;
		Scores scores;

		scores.load();

		if (argc == 3) {
			Setup setup(screen, scores);
			setup.play(game_index, difficulty_index);
		} else {
			Menu menu(screen, scores);
			menu.run();
		}
	} catch (Quit q) {
		return 0;
	} catch (Screen::Too_small e) {
		cerr << "terminal is too small.\n";
		cerr << "minimum required size: " << Screen::MAX_ROW + 2 <<
						" rows by " << Screen::MAX_COL + 2 << " columns.\n";
		cerr << "current size: " << e.rows << " rows by "<< e.cols <<
						" columns.\n";
		return 1;
	} catch (Argument e) {
		cerr << "usage: " << argv[0] << " [<game> <difficulty>]\n";
		return 1;
	} catch (exception e) {
		cerr << "application stopped after a critical error.\n";
		cerr << "error message: " << e.what() << "\".\n";
		return 1;
	} catch (...) {
		cerr << "application stopped after an unknown critical error.\n";
		return 1;
	}
}
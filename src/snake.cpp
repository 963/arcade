#include "snake.h"
#include <cmath>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cassert>

using namespace std;





Snake::Snake(Screen &screen_in)
: Speed_game{screen_in} {}



Snake::~Snake() {}



const size_t Snake::GAME_ROWS = Screen::MAX_ROW;
const size_t Snake::GAME_COLS = Screen::MAX_COL - 10;
const size_t Snake::START_LENGTH = 3;
const size_t Snake::START_COL = 15;
const string Snake::SHRINKER_CHARS =
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
const size_t Snake::SHRINKER_SIZE = 3;
const size_t Snake::SHRINKER_RARITY = 20;
const size_t Snake::SHRINKER_POWER = 5;
const char Snake::SKIN = '#';
const char Snake::FOOD = '@';



void Snake::prepare() {
	// Screen
	screen.clear();

	// Bar
	for (size_t row = 0; row < GAME_ROWS; ++row) {
		screen(row, GAME_COLS) = '|';
	}
	screen.print(1, GAME_COLS+2, "score:");
	screen.print(4, GAME_COLS+2, "length:");
	screen.print(7, GAME_COLS+2, "fill:");

	// Snake
	queue<Point<size_t>>{}.swap(body);
	head.row = GAME_ROWS / 2 + GAME_ROWS % 2 - 1;
	head.col = START_COL - 1;
	for (size_t i = 0; i < START_LENGTH; ++i) {
		++head.col;
		body.push(head);
		screen(head.row, head.col) = SKIN;
	}

	// Food and shrinker
	place_food();
	shrinker_age = SHRINKER_CHARS.size();

	// Direction
	dir = Direction::RIGHT;
	dir_old = dir;

	// Score
	score = 0;
}



Snake::Interval Snake::get_interval(size_t difficulty) {
	double framerate = pow(1.5, static_cast<double>(difficulty)) * 2;
	return Interval(1000 / framerate);
}



void Snake::handle_keypress(Screen::Key key) {
	switch (key) {
		case Screen::Key::UP:
			if (dir_old != Direction::DOWN) dir = Direction::UP;
			break;
		case Screen::Key::LEFT:
			if (dir_old != Direction::RIGHT) dir = Direction::LEFT;
			break;
		case Screen::Key::DOWN:
			if (dir_old != Direction::UP) dir = Direction::DOWN;
			break;
		case Screen::Key::RIGHT:
			if (dir_old != Direction::LEFT) dir = Direction::RIGHT;
			break;
		case Screen::Key::SPACE:
		case Screen::Key::OTHER:
			{}
	}
}



bool Snake::tick() {
	if (body.size() == Screen::MAX_ROW * Screen::MAX_COL) {
		score += 100;
		update_bar();
		return true;
	}

	switch (dir) {
		case Direction::UP:
			head.row = (head.row == 0) ? (GAME_ROWS - 1) : (head.row - 1);
			break;
		case Direction::LEFT:
			head.col = (head.col == 0) ? (GAME_COLS - 1) : (head.col - 1);
			break;
		case Direction::DOWN:
			head.row = (head.row == GAME_ROWS - 1) ? 0 : (head.row + 1);
			break;
		case Direction::RIGHT:
			head.col = (head.col == GAME_COLS - 1) ? 0 : (head.col + 1);
	}
	
	dir_old = dir;

	switch (screen(head.row, head.col)) {
		case SKIN:
			update_bar();
			return true;
		case FOOD:
			body.push(head);
			screen(head.row, head.col) = SKIN;
			++score;
			place_food();
			// Place a shrinker if needed
			if (shrinker_age == SHRINKER_CHARS.size()) {
				if (!ran.get_size_t(0, SHRINKER_RARITY)) {
					place_shrinker();
				}
			}
			break;
		case ' ':
			body.push(head);
			screen(head.row, head.col) = SKIN;
			screen(body.front().row, body.front().col) = ' ';
			body.pop();
			break;
		default:
			// Hit shrinker!
			body.push(head);
			screen(head.row, head.col) = SKIN;
			for (size_t i = 0; i < SHRINKER_POWER+1; ++i) {
				if (body.size() <= START_LENGTH) break;
				screen(body.front().row, body.front().col) = ' ';
				body.pop();
			}
			shrinker_age = SHRINKER_CHARS.size();
	}

	draw_shrinker();
	age_shrinker();

	update_bar();
	return false;
}



void Snake::place_food() {
	vector<Point<size_t>> empty;
	empty.reserve(GAME_ROWS * GAME_COLS);
	for (size_t i = 0; i < GAME_ROWS; ++i) {
		for (size_t j = 0; j < GAME_COLS; ++j) {
			if (screen(i, j) == ' ') {
				empty.emplace_back(i, j);
			}
		}
	}
	assert(!empty.empty());
	size_t place = ran.get_size_t(0, empty.size());
	Point<size_t> food_place = empty[place];
	screen(food_place.row, food_place.col) = FOOD;
}



void Snake::place_shrinker() {
	// Prepare a vector for empty spaces
	vector<Point<size_t>> empty;
	empty.reserve((GAME_ROWS-SHRINKER_SIZE+1) * (GAME_COLS-SHRINKER_SIZE+1));

	// Loop through all potential top-left corners
	for (size_t row = 0; row < GAME_ROWS-SHRINKER_SIZE+1; ++row) {
		for (size_t col = 0; col < GAME_COLS-SHRINKER_SIZE+1; ++col) {
			// Check if shrinker space is empty
			bool good = true;
			for (size_t i = 0; i < SHRINKER_SIZE; ++i) {
				for (size_t j = 0; j < SHRINKER_SIZE; ++j) {
					good = good && (screen(row+i, col+j) == ' ');
				}
			}
			// If space is empty, add it to the array
			if (good) {
				empty.emplace_back(row, col);
			}
		}
	}

	// Nowhere to place?
	if (empty.empty()) return;

	// Place a shrinker - finally!
	size_t place = ran.get_size_t(0, empty.size());
	shrinker_pos = empty[place];
	shrinker_age = 0;
}



void Snake::age_shrinker() {
	// Check REQUIRES
	assert(shrinker_age <= SHRINKER_CHARS.size());

	if (shrinker_age < SHRINKER_CHARS.size()) ++shrinker_age;
}



void Snake::draw_shrinker() {
	for (size_t i = 0; i < SHRINKER_SIZE; ++i) {
		for (size_t j = 0; j < SHRINKER_SIZE; ++j) {
			if (shrinker_age == SHRINKER_CHARS.size()) {
				// Dead
				char &curr = screen(shrinker_pos.row+i, shrinker_pos.col+j);
				if (curr != SKIN && curr != FOOD) curr = ' ';
			} else {
				// Alive
				screen(shrinker_pos.row+i, shrinker_pos.col+j) =
								SHRINKER_CHARS[shrinker_age];
			}
		}
	}
}



void Snake::update_bar() {
	// Clear
	string spaces;
	spaces.resize(Screen::MAX_COL - GAME_COLS - 1, ' ');
	for (size_t row : {2,5,8}) {
		screen.print(row, GAME_COLS+1, spaces);
	}

	// Easy stuff - score and length
	screen.print(2, GAME_COLS+2, to_string(score*multiplier));
	screen.print(5, GAME_COLS+2, to_string(body.size()));

	// Hard stuff - fill
	double fill = 100*static_cast<double>(body.size()) / (GAME_ROWS*GAME_COLS);
	ostringstream fill_str;
	fill_str << fixed << setprecision(2) << fill << '%';
	screen.print(8, GAME_COLS+2, fill_str.str());
}
#ifndef MENU_H
#define MENU_H

#include "lists.h"
#include "screen.h"
#include "score.h"



class Menu {
public:

	// M: this
	// E: Creates a menu with given screen and scores
	Menu(Screen &screen_in, Scores &scores_in);

	// M: this, screen, keyboard
	// E: Lets the user select and play multiple games
	void run();

private:

	// Variables

	Screen &screen;
	Scores &scores;

	size_t game_index, difficulty_index;
	int current_score;
	bool game_change, difficulty_change, want_menu;

	// Functions

	// M: this->{game_index, want_menu}, screen, keyboard
	// E: Shows a game selection screen and lets user select a game
	void get_game();

	// M: this->{difficulty_index, want_menu}, screen, keyboard
	// E: Shows a difficulty selection screen and lets user select a difficulty
	void get_difficulty();

	// M: this->game_change, this->difficulty_change, screen, keyboard
	// E: Shows scores and lets the user select next action
	void show_scores();

	Menu() = delete;

};

#endif
#ifndef SCREEN_H
#define SCREEN_H

// Usage:
//     Screen screen;
//     screen(row, col) = char;
//     screen.update();
//
//     Screen::Key key = Screen::Key::OTHER;
//     while (screen.get_key(key, false)) {
//         switch (key) {
//             case Screen::Key::UP:
//                 do_something();
//                 break;
//             ...
//         }
//     }
//
//     Screen::Key key = Screen::Key::OTHER;
//     screen.get_key(key, true);





#include <vector>
#include <string>
#include <exception>



class Screen {
public:

	// Types

	enum class Key {UP, LEFT, DOWN, RIGHT, SPACE, OTHER};

	class Too_small : public std::exception {
	public:
		Too_small(size_t rows_in, size_t cols_in);

		size_t rows, cols;

		Too_small() = delete;
	};

	// Variables

	static const size_t MAX_ROW = 22;
	static const size_t MAX_COL = 78;

	// Functions

	// M: this, terminal settings, ncurses, program flow (exception)
	// E: Initialises a screen, throws Small_screen if necessary
	Screen();

	// R: row < MAX_ROW && col < MAX_COL
	// M: this
	// E: Accesses specified pixel
	char & operator() (size_t row, size_t col);

	// R: row < MAX_ROW && col < MAX_COL
	// E: Accesses specified pixel
	const char & operator() (size_t row, size_t col) const;

	// M: this 
	// E: Clears screen
	void clear();

	// R: row < MAX_ROW && col < MAX_COL &&
	//    to_print.length() < distance to MAX_COL + 1
	// M: this
	// E: Prints given string to specified coordinate
	void print(size_t row, size_t col, const std::string &to_print);

	// M: Terminal screen
	// E: Updates terminal screen, checks terminal size, , throws Small_screen
	//    if necessary
	void update() const;

	// M: keyboard, program flow (Quit exception)
	// E: If wait is false, sets key to pressed key and returns true, or leaves
	//    key unchanged and returns false if nothing is pressed. If wait is
	//    true, waits for keypress, sets key to pressed key, and returns and
	//    undefined bool. Pauses or quits if necessary
	bool get_key(Key &key, bool wait) const;

	// M: this, terminal settings, ncurses
	// E: Destroys screen
	~Screen();

private:

	std::vector<std::string> data;
	mutable size_t console_rows, console_cols;

	// M: keyboard, program flow (Quit exception)
	// E: Pauses the program, then looks for keypresses and unpauses on 'p' or
	//    quits on 'q'
	void pause() const;

};

#endif
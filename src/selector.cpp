#include "selector.h"
#include <chrono>
#include <thread>
#include <cassert>

using namespace std;





Selector::Selector(Screen &screen_in)
: screen{screen_in} {}



size_t Selector::show(const vector<string> &options, size_t start_row) {
	options_ptr = &options;
	start_row_copy = start_row;

	assert(check_invariants());

	print_labels();
	select_first();
	ask_user();

	return curr_option;
}



bool Selector::check_invariants() const {
	if (start_row_copy + options_ptr->size() > Screen::MAX_ROW) return false;
	for (const auto &str : *options_ptr) {
		if (5 + str.length() > Screen::MAX_COL) return false;
	}
	for (const auto &str : *options_ptr) {
		if (str.length()) return true;
	}
	return false;
}



void Selector::print_labels() {
	for (size_t i = 0; i < options_ptr->size(); ++i) {
		screen.print(start_row_copy+i, 5, (*options_ptr)[i]);
	}
}



void Selector::select_first() {
	for (curr_option = 0; curr_option < options_ptr->size(); ++curr_option) {
		if ((*options_ptr)[curr_option].length()) break;
	}
	assert(curr_option < options_ptr->size());
	old_option = curr_option;
	draw_arrow();
}



void Selector::ask_user() {
	Screen::Key key = Screen::Key::OTHER;
	while (true) {
		screen.get_key(key, true);
		switch (key) {
			case Screen::Key::UP:
				move_up();
				break;
			case Screen::Key::LEFT:
				break;
			case Screen::Key::DOWN:
				move_down();
				break;
			case Screen::Key::RIGHT:
				break;
			case Screen::Key::SPACE:
				return;
			case Screen::Key::OTHER:
				{}
		}
	}
}



void Selector::move_up() {
	for (size_t i = curr_option; i--;) {
		if ((*options_ptr)[i].length()) {
			curr_option = i;
			draw_arrow();
			break;
		}
	}
}



void Selector::move_down() {
	for (size_t i = curr_option+1; i < options_ptr->size(); ++i) {
		if ((*options_ptr)[i].length()) {
			curr_option = i;
			draw_arrow();
			break;
		}
	}
}



void Selector::draw_arrow() {
	screen.print(start_row_copy + old_option, 2, "  ");
	screen.print(start_row_copy + curr_option, 2, "->");
	old_option = curr_option;
	screen.update();
}
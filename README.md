# The Terminal Arcade Machine

Hello, and welcome to the official repository for the Terminal Arcade Machine,
the best way to waste time in your console!

## Installation

For now, arcade only works on Unix-based operating systems, like Linux or MacOS.
So, if you have Windows, you are kinda screwed, sorry about that.

To install arcade, follow the instructions for your operating system, and it
should be a breeze. If you encounter any problems, feel free to
[raise an issue](https://gitlab.com/Alexey_Nigin/arcade/issues) or contact one
of us directly.

### Linux

These instructions are for Ubuntu / Debian, but if you have a different Linux
distribution, it should be pretty similar.

1.	Open the terminal.

2.	Install `make` and `g++`:

	```shell
	sudo apt install make g++
	```

3.	Install ncurses:

	```shell
	sudo apt install libncurses5-dev libncursesw5-dev
	```

4.	Get the source code for arcade. To do that, either clone the repo or
	download one of the provided tarballs and extract the files.

5.	`cd` to the directory with the files and run `make`.

6.	Run `./arcade`.

7.	Enjoy!

### MacOS

1.	Open the terminal.

2.	Install developer tools:
	
	```shell
	xcode-select --install
	```

	When you get a popup, select "Install".

3.	Install Homebrew:

	```shell
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	```

4.	Install ncurses:

	```shell
	brew install ncurses
	```

	If the above command doesn't work for you, try this:

	```shell
	brew install homebrew/dupes/ncurses
	```

5.	Get the source code for arcade. To do that, either clone the repo or
	download one of the provided tarballs and extract the files.

6.	`cd` to the directory with the files and run `make`. You might get a few
	warnings; ignore them.

7.	Run `./arcade`.

8.	Enjoy!

## Gameplay

All games in the Terminal Arcade Machine use the same controls: four directional
keys (WASD / arrows) and one action key (space / enter). You can also use Q to
quit and P to pause.

We currently have four cool games:

*	**block_blast** - Destroy evil blocks before they get to the bottom of the
	screen! Use left and right directional keys to move your cannon, and fire
	with the action key. There are four types of blocks, and four corresponding
	types of bullets. If you hit a matching block, it will be blasted to
	oblivion, but beware: if you miss, the bullet will stick to the block,
	making your life way harder!

*	**hedgehog** - Encircle islands to collect points, while also dodging hordes
	of enemies! Use directional keys to move around, and press the action key
	all you want because it doesn't do anything in this game. To get points for
	an island, you have to run along all of its perimeter. You'll receive more
	points if the island is big and if you are collecting points from it for the
	first time, keep that in mind!

*	**snake** - Collect food and try not to run into your tail! Use directional
	keys to turn. Every bit of food makes you one block longer. But don't
	despair: you can eat flashing shrinkers to get short again!

*	**space_trip** - Dodge obstacles or blast through them! Use up and down
	directional keys to move around, left/right keys to fire small bullets, and
	the action key to activate the giant laser. Shooting consumes power: one
	unit of power for a bullet and five for a laser beam. Power regenerates very
	slowly, so don't waste it!

## Credits

The Terminal Arcade Machine was made by Alexey Nigin (epotek-at-yandex-dot-ru)
and Manjari Trivedi (trivm-at-umich-dot-edu). We are just two college students
who wanted to get better at C++ and figured that making a terminal game would be
a fun way to do so. If you like the way we wrote arcade and you happen to be
hiring software developers, send us an email and we'll work for you!

The Terminal Arcade Machine is available under the
[Creative Commons Attribution-NonCommercial License](https://creativecommons.org/licenses/by-nc/4.0/).
